import fs from 'node:fs'
import path from 'node:path'
import test from 'tape'
import md from 'markdown-it'
import {legacySyntax as syntax} from '../dev/index.js'

function parse(text, options) {
  const parser = md().use(syntax, options)
  return parser.render(text)
}

test('markdown -> html (micromark)', (t) => {
  t.deepEqual(
    parse('#heading#'),
    '<h1>heading</h1>\n',
    'should parse legacy h1 headings'
  )
  t.deepEqual(
    parse('# heading#'),
    '<h1>heading</h1>\n',
    'should parse legacy h1 headings with prefix whitespace'
  )
  t.deepEqual(
    parse('#heading #'),
    '<h1>heading</h1>\n',
    'should parse legacy h1 headings with suffix whitespace'
  )
  t.deepEqual(
    parse('# heading #'),
    '<h1>heading</h1>\n',
    'should parse legacy h1 headings with prefix and suffix whitespace'
  )
  t.deepEqual(
    parse('##heading##'),
    '<h2>heading</h2>\n',
    'should parse legacy h2 headings'
  )
  t.deepEqual(
    parse('## heading##'),
    '<h2>heading</h2>\n',
    'should parse legacy h2 headings with prefix whitespace'
  )
  t.deepEqual(
    parse('##heading ##'),
    '<h2>heading</h2>\n',
    'should parse legacy h2 headings with suffix whitespace'
  )
  t.deepEqual(
    parse('## heading ##'),
    '<h2>heading</h2>\n',
    'should parse legacy h2 headings with prefix and suffix whitespace'
  )
  t.deepEqual(
    parse("#!\"§$%&/()=?{}[]\\'`°^+*~'’,;.:-_<>|#"),
    "<h1>!&quot;§$%&amp;/()=?{}[]'`°^+*~'’,;.:-_&lt;&gt;|</h1>\n",
    'should parse headings containing punctuation'
  )
  t.deepEqual(
    parse('#äöüßÄÖÜẞ#'),
    '<h1>äöüßÄÖÜẞ</h1>\n',
    'should parse german mutated vowels'
  )
  t.deepEqual(
    parse('#heading\n'),
    '<p>#heading</p>\n',
    'should not allow newlines in heading'
  )

  t.deepEqual(
    parse('###heading###'),
    '<p>###heading###</p>\n',
    'should not parse h3 headings'
  )

  t.deepEqual(
    parse('##heading#'),
    '<p>##heading#</p>\n',
    'should not allow less closing than opening fences'
  )
  t.deepEqual(
    parse('#heading##'),
    '<p>#heading##</p>\n',
    'should not allow more closing than opening fences'
  )

  t.deepEqual(
    parse('#h e a d i n g    #'),
    '<h1>h e a d i n g</h1>\n',
    'should cut off suffix whitespaces'
  )

  t.deepEqual(
    parse('# heading 1'),
    '<h1>heading 1</h1>\n',
    'should not interfere with standard md headings (1)'
  )

  t.deepEqual(
    parse('## heading 2'),
    '<h2>heading 2</h2>\n',
    'should not interfere with standard md headings (2)'
  )

  t.deepEqual(
    parse('### heading 3'),
    '<h3>heading 3</h3>\n',
    'should not interfere with standard md headings (3)'
  )

  t.deepEqual(
    parse('#### heading 4'),
    '<h4>heading 4</h4>\n',
    'should not interfere with standard md headings (4)'
  )

  t.deepEqual(
    parse('##### heading 5'),
    '<h5>heading 5</h5>\n',
    'should not interfere with standard md headings (5)'
  )

  t.deepEqual(
    parse('###### heading 6'),
    '<h6>heading 6</h6>\n',
    'should not interfere with standard md headings (6)'
  )

  t.deepEqual(
    parse(
      '    #all sorts of#\n    # block indented#\n    #headings #\n    ##are ignored##'
    ),
    '<pre><code>#all sorts of#\n# block indented#\n#headings #\n##are ignored##\n</code></pre>\n',
    'Should ignore headings in block indentation'
  )

  t.end()
})

test('fixtures', (t) => {
  const base = path.join('test', 'fixtures')
  const files = fs.readdirSync(base).filter((d) => /\.md$/.test(d))
  let index = -1

  while (++index < files.length) {
    const name = path.basename(files[index], '.md')
    const input = fs.readFileSync(path.join(base, name + '.md'), 'utf8')
    const actual = parse(input)

    let expected

    try {
      expected = String(fs.readFileSync(path.join(base, name + '.html')))
    } catch {}

    if (expected) {
      t.deepEqual(actual, expected, name)
    } else {
      fs.writeFileSync(path.join(base, name + '.html'), actual)
    }
  }

  t.end()
})

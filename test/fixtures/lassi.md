Ein erfrischendes Sommergetränk, welches Du als Zwischenmahlzeit oder als Vorspeise genießen kannst.

#Zutaten für 2 Personen#
* 250g Kirschen
* 1TL Rosenwasser
* 4 EL veganer Kokosjoghurt
* 300ml Reis-Kokos-Drink
* 1/4 TL Vanille

#Und so einfach geht's#

**Schritt 1**
Achte darauf, dass der Kokosjoghurt und der Reis-Kokos-Drink Zimmertemperatur haben.

**Schritt 2**
Die Kirschen waschen, entsteinen und in ein hohes Gefäß oder in einen Standmixer geben.

**Schritt 3**
Alle restlichen Zutaten hinzugeben und mit einem Pürierstab oder Mixer cremig pürieren.

##Nachgekocht?##

Dann teile gern Deine Fotos mit [@salufast](https://www.instagram.com/salufast/) auf Instagram und nutze das Hashtag **#seidirwichtig.**

const codes = {
  numberSign: 0x23,
  cr: 0x0d,
  lf: 0x0a
}

export function legacySyntax(md) {
  md.block.ruler.before('heading', 'legacyHeading', legacyHeadingCall, [
    'paragraph',
    'reference',
    'blockquote'
  ])
  function legacyHeadingCall(state, startLine, endLine, silent) {
    let ch
    let level
    let token
    let pos = state.bMarks[startLine] + state.tShift[startLine]
    let max = state.eMarks[startLine]

    ch = state.src.charCodeAt(pos)

    if (ch !== codes.numberSign || pos >= max) {
      return false
    }

    // Count heading level
    level = 1
    ch = state.src.charCodeAt(++pos)
    while (ch === codes.numberSign && pos < max && level <= 2) {
      level++
      ch = state.src.charCodeAt(++pos)
    }

    if (level > 2) {
      return false
    }

    /* c8 ignore next 3 */
    if (silent) {
      return true
    }

    // Let's cut tails like '    ###  ' from the end of string
    max = state.skipSpacesBack(max, pos)
    const temporary = state.skipCharsBack(max, codes.numberSign, pos)
    if (temporary + level !== max) return false

    if (temporary > pos) {
      max = temporary
    }

    state.line = startLine + 1

    token = state.push('heading_open', 'h' + String(level), 1)
    token.markup = '########'.slice(0, level)
    token.map = [startLine, state.line]

    token = state.push('inline', '', 0)
    token.content = state.src.slice(pos, max).trim()
    token.map = [startLine, state.line]
    token.children = []

    token = state.push('heading_close', 'h' + String(level), -1)
    token.markup = '########'.slice(0, level)

    return true
  }
}

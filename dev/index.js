import {legacySyntax as syntax} from './lib/syntax.js'

export const legacySyntax = syntax
export default syntax
